
import React, { useEffect, useState } from 'react';

import { airportData } from './utils/constant'
import DistanceBetween from './utils/DistanceBetween'
import SearchField from './components/SearchField';
import 'antd/dist/antd.css';
import './App.css';

function App() {
  const [data, setData] = useState([])
  const [value1, setValue1] = useState(null);
  const [value2, setValue2] = useState(null);
  const [distance, setDistance] = useState(null)
  const [dataSelected, setDataSelected] = useState(null)
  const [error, setError] = useState(null)

  useEffect(() => {
    const dataToSet = airportData.map((item) => ({
      ...item,
      search: item.search.split("|"),
    }));
    setData(dataToSet)
  }, [])

  useEffect(() => {
    if (value1 !== null && value2 !== null) {
      if (value1 === value2) {
        setError("Cant select same location for airport")
      } else {
        setError(null)
      }
    } else {
      setDataSelected(null)
      setDistance(null)
    }
  }, [value1, value2])

  const submit = () => {
    if (error !== null) {
      return
    }
    if (value1 !== null && value2 !== null) {
      const firstCode = value1.split("-")[0]
      const secondCode = value2.split("-")[0]
      const selectData = airportData.filter((item) => item.code === firstCode || item.code === secondCode)
      const distance = DistanceBetween(selectData[0].lat, selectData[0].lng, selectData[1].lat, selectData[1].lng)
      setDataSelected(selectData)
      setDistance(distance)
      setError(null)
    } else {
      setError("both field is required")
      setDataSelected(null)
      setDistance(null)
    }
  }

  return (
    <div className='main-wrapper'>
      <div className='container'>
        <div className='card'>
          <h1 className='main-heading'>Distance Between Two Airports</h1>
          <div className='main-fields-wrapper'>
            <SearchField label="Airport 1" data={data} setValue={setValue1} placeholder="From" />
            <SearchField label="Airport 2" data={data} setValue={setValue2} placeholder="To" />
          </div>
          <div className='button-wrapper'>
            <button className="submit-button" onClick={() => submit()}>submit</button>
          </div>
          {distance && <p className='distance'>{`Distance From ${dataSelected[0].name} To ${dataSelected[1].name} is : ${distance.toFixed(2)} KM`}</p>}
          {error && <p className='error'>{error}</p>}
        </div>
      </div>
    </div>
  );
}

export default App;

import React, { useState } from 'react'
import { AutoComplete } from 'antd';
import Debounced from '../utils/Debounced'

const { Option } = AutoComplete;

const SearchField = ({ label, data, setValue, placeholder }) => {

    const [options, setOptions] = useState([]);
    const [searchString, setSearchString] = useState("")

    function onSearch(searchText) {
        if (searchText === "") {
            setOptions([])
            return
        }
        function isPresent(searchData) {
            return searchData.includes(searchText)
        }
        const result = data.filter((item) => {
            const dataTosearch = JSON.parse(JSON.stringify(item))
            return dataTosearch.search.some(isPresent)
        })
        const option = result.map((item) => {
            return { value: `${item.code}-${item.name}`, label: `${item.code}-${item.name}` }
        })
        setOptions(option)
    };

    const onSelect = (data) => {
        setValue(data);
    };

    const onChange = (data) => {
        setSearchString(data);
    };

    const debouncedSearch = Debounced(onSearch, 400)

    return (
        <div className='fiels'>
            <p>{label}</p>
            <AutoComplete
                value={searchString}
                options={options}
                className="w-80"
                onSelect={onSelect}
                onSearch={debouncedSearch}
                onChange={onChange}
                placeholder={placeholder}
                allowClear={true}
                onClear={() => setValue(null)}
            >
                {options?.map(({ label, value }, index) => (
                    <Option key={index} value={value}>
                        {label}
                    </Option>
                ))}
            </AutoComplete>
        </div>
    )
}

export default SearchField